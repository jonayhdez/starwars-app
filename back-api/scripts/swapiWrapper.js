const mongoose = require('mongoose');

const DB_URL = require('../db');
const fetch = require('node-fetch');

const People = require('../models/People');
const Planets = require('../models/Planets');
const Films = require('../models/Films');
const Species = require('../models/Species');
const Vehicles = require('../models/Vehicles');
const Starships = require('../models/Starships');

let countData='0';
let page=0;
let charsData=[];
let planetsData=[];
let filmsData=[];
let speciesData=[];
let vehiclesData=[];
let starshipsData=[];

const loading = (count)=>{
    let page=count/10;
    if (page % 1 !== 0){
        page++;
    }
    return parseInt(page);
}

const takeId = (data)=>{
    return data.split("/")[data.split("/").length - 2];
}

const formatElement = (element) =>{
    const data=[];
    element.forEach(e =>{
        data.push(takeId(e));
    });
    return data;
}

const scrapApiPeople = () => {
    return new Promise((resolve, reject) => {
        let characters = [];
        page = 1;
        console.log("Downloading characters...")
        const getChars = (url = "https://swapi.dev/api/people/") => {
            fetch(url).then(res => {
                return res.json();
            })
                .then(res => {
                    countData = res.count;
                    characters.push(res.results);
                    const countPages = loading(countData);
                    console.clear();
                    console.log(`Downloading ${countData} characters... page ${page} of ${countPages}`);
                    page++;
                    if (res.next) {
                        getChars(res.next);
                    } else {
                        let charArray=[];

                        characters.forEach(charPage =>{
                            charPage.forEach(char =>{
                                const idChar = { id: parseInt(takeId(char.url)) };
                                const worldId = takeId(char.homeworld);
                                const filmsId = formatElement(char.films);
                                const speciesId = formatElement(char.species);
                                const vehiclesId=formatElement(char.vehicles);
                                const starshipsId=formatElement(char.starships);
                                const imgChar={img:[]};
                                const affiliate={affilliations: []};

                                char.films=filmsId;
                                char.homeworld=worldId;
                                char.species=speciesId;
                                char.vehicles=vehiclesId;
                                char.starships=starshipsId;
                                delete char.url;
                                charArray.push({...idChar,...char,...imgChar,...affiliate});
                            })
                        })
                        console.log("Characters downloaded... done")
                        resolve(charArray);
                    }
                });
        }
        getChars();
    });
};

const scrapApiPlanets = () => {
    return new Promise((resolve, reject) => {
        let planets = [];
        page=1;
        console.log("Downloading planets...")
        const getPlanets = (url = "https://swapi.dev/api/planets/") => {
            fetch(url).then(res => {
                return res.json();
            })
                .then(res => {
                    countData=res.count;
                    planets.push(res.results);
                    const countPages = loading(countData);
                    console.clear();
                    console.log(`Downloading ${countData} planets... page ${page} of ${countPages}`);
                    page++;
                    if (res.next) {
                        getPlanets(res.next);
                    } else {
                        const planetsArray=[];
                        planets.forEach(planetsPage =>{
                            planetsPage.forEach(planet =>{
                                const idPlanet={ id: parseInt(takeId(planet.url)) };
                                const residentsId = formatElement(planet.residents);
                                const filmsId = formatElement(planet.films);
                                const imgPlanet={img:[]};
                                planet.films=filmsId;
                                planet.residents=residentsId;
                                delete planet.url;
                                planetsArray.push({...idPlanet,...planet,...imgPlanet});
                            })
                        })
                        console.log("Downloading planets... done")
                        resolve(planetsArray);
                    }
                });
        }

        getPlanets();
    });
};

const scrapApiFilms = () => {
    return new Promise((resolve, reject) => {
        let films = [];
        page=1;
        console.log("Downloading films...")
        const getFilms = (url = "https://swapi.dev/api/films/") => {
            fetch(url).then(res => {
                return res.json();
            })
                .then(res => {
                    countData=res.count;
                    films.push(res.results);
                    const countPages = loading(countData);
                    console.clear();
                    console.log(`Downloading ${countData} films... page ${page} of ${countPages}`);
                    page++;
                    if (res.next) {
                        getFilms(res.next);
                    } else {
                        const filmsArray=[];
                        films.forEach(filmsPage =>{
                            filmsPage.forEach(film =>{
                                const idFilm={ id: parseInt(takeId(film.url)) };
                                const charsId = formatElement(film.characters);
                                const planetsId = formatElement(film.planets);
                                const starshipsId = formatElement(film.starships);
                                const vehiclesId = formatElement(film.vehicles);
                                const speciesId = formatElement(film.species);
                                const imgFilms={img:[]};
                                film.characters=charsId;
                                film.planets=planetsId;
                                film.starships=starshipsId;
                                film.vehicles=vehiclesId;
                                film.species=speciesId;
                                delete film.url;
                                filmsArray.push({...idFilm,...film,...imgFilms});
                            })
                        })
                        console.log("Downloading films... done")
                        resolve(filmsArray);
                    }
                });
        }

        getFilms();
    });
};

const scrapApiSpecies = () => {
    return new Promise((resolve, reject) => {
        let species = [];
        page=1;
        console.log("Downloading species...")
        const getSpecies = (url = "https://swapi.dev/api/species/") => {
            fetch(url).then(res => {
                return res.json();
            })
                .then(res => {
                    countData=res.count;
                    species.push(res.results);
                    const countPages = loading(countData);
                    console.clear();
                    console.log(`Downloading ${countData} species... page ${page} of ${countPages}`);
                    page++;
                    if (res.next) {
                        getSpecies(res.next);
                    } else {
                        const speciesArray=[];
                        species.forEach(speciesPage =>{
                            speciesPage.forEach(specie =>{
                                const idSpecie={ id: parseInt(takeId(specie.url)) };
                                const charsId = formatElement(specie.people);
                                let planetsId = '';
                                if (specie.homeworld != null){
                                    planetsId = takeId(specie.homeworld);
                                }
                                const filmsId = formatElement(specie.films);
                                const imgSpecie={img:[]};
                                specie.people=charsId;
                                specie.homeworld=planetsId;
                                specie.films=filmsId;
                                delete specie.url;
                                speciesArray.push({...idSpecie,...specie,...imgSpecie});
                            })
                        })
                        console.log("Downloading species... done")
                        resolve(speciesArray);
                    }
                });
        }

        getSpecies();
    });
};

const scrapApiVehicles = () => {
    return new Promise((resolve, reject) => {
        let vehicles = [];
        page=1;
        console.log("Downloading vehicles...")
        const getVehicles = (url = "https://swapi.dev/api/vehicles/") => {
            fetch(url).then(res => {
                return res.json();
            })
                .then(res => {
                    countData=res.count;
                    vehicles.push(res.results);
                    const countPages = loading(countData);
                    console.clear();
                    console.log(`Downloading ${countData} vehicles... page ${page} of ${countPages}`);
                    page++;
                    if (res.next) {
                        getVehicles(res.next);
                    } else {
                        const vehiclesArray=[];
                        vehicles.forEach(vehiclesPage =>{
                            vehiclesPage.forEach(vehicle =>{
                                const idVehicle={ id: parseInt(takeId(vehicle.url)) };
                                const charsId = formatElement(vehicle.pilots);
                                const filmsId = formatElement(vehicle.films);
                                const imgVehicle={img:[]};
                                vehicle.pilots=charsId;
                                vehicle.films=filmsId;
                                delete vehicle.url;
                                vehiclesArray.push({...idVehicle,...vehicle,...imgVehicle});
                            })
                        })
                        console.log("Downloading vehicles... done")
                        resolve(vehiclesArray);
                    }
                });
        }

        getVehicles();
    });
};

const scrapApiStarships = () => {
    return new Promise((resolve, reject) => {
        let starships = [];
        page=1;
        console.log("Downloading starships...")
        const getStarships = (url = "https://swapi.dev/api/starships/") => {
            fetch(url).then(res => {
                return res.json();
            })
                .then(res => {
                    countData=res.count;
                    starships.push(res.results);
                    const countPages = loading(countData);
                    console.clear();
                    console.log(`Downloading ${countData} starships... page ${page} of ${countPages}`);
                    page++;
                    if (res.next) {
                        getStarships(res.next);
                    } else {
                        const starshipsArray=[];
                        starships.forEach(starshipsPage =>{
                            starshipsPage.forEach(starship =>{
                                const idStarship={ id: parseInt(takeId(starship.url)) };
                                const charsId = formatElement(starship.pilots);
                                const filmsId = formatElement(starship.films);
                                const imgStarship={img:[]};
                                starship.pilots=charsId;
                                starship.films=filmsId;
                                delete starship.url;
                                starshipsArray.push({...idStarship,...starship,...imgStarship});
                            })
                        })
                        console.log("Downloading starships... done")
                        resolve(starshipsArray);
                    }
                });
        }

        getStarships();
    });
};

console.log("Deleting data base...")

mongoose.connect(DB_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(async  ()=>{
        console.clear();
        console.log("Starting app...")
        charsData = await scrapApiPeople();
        planetsData = await scrapApiPlanets();
        filmsData = await scrapApiFilms();
        speciesData = await scrapApiSpecies();
        vehiclesData = await scrapApiVehicles();
        starshipsData = await scrapApiStarships();

        console.clear()
        await People.collection.drop();
        await Planets.collection.drop();
        await Films.collection.drop();
        await Species.collection.drop();
        await Vehicles.collection.drop();
        await Starships.collection.drop();

        console.log('Data base... deleted');
    })
    .catch((err)=>{
        console.log(err.message);
    })
    .then(async ()=>{
        console.clear()
        await People.create(charsData);
        console.log("Characters added");
        await Planets.create(planetsData);
        console.log("planets added");
        await Films.create(filmsData);
        console.log("films added");
        await Species.create(speciesData);
        console.log("species added");
        await Vehicles.create(vehiclesData);
        console.log("vehicles added");
        await Starships.create(starshipsData);
        console.log("Starships added");

        console.log("Process finished... success")
        console.log("Do you want to add default images? (y/n)");

        const keypress = require('keypress');
        const tty = require('tty');

        keypress(process.stdin);
        process.stdin.on('keypress', async(ch, key)=> {
            if (key.name === 'y'){
                console.log("Uploading Seeds...")
                const load = require('../seeds/seedImages');
                await load();

                console.log("Closing app...")
                process.exit(0);
            }else if (key.name === 'n'){
                console.log("Closing app...")
                process.exit(0);
            }

        });
        if (typeof process.stdin.setRawMode == 'function') {
            process.stdin.setRawMode(true);
        } else {
            tty.setRawMode(true);
        }

        process.stdin.resume();

    })
    .catch((err)=>{
        console.log(err.message);
    })
