const express = require ('express');
require('./db.js');

const passport = require('passport');
require('./passport');

const path = require("path");

const PORT = 3005;
const server = express();
const router = express.Router();

const loginRoutes = require('./routes/login.routes');
const peopleRoutes = require('./routes/people.routes');
const planetsRoutes = require('./routes/planets.routes');
const filmsRoutes = require('./routes/films.routes');
const speciesRoutes = require('./routes/species.routes');
const vehiclesRoutes = require('./routes/vehicles.routes');
const starshipsRoutes = require('./routes/starships.routes');


server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(express.static(path.join(__dirname, 'public')));

server.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

server.use(passport.initialize());

server.use('/login', loginRoutes);
server.use('/people', peopleRoutes);
server.use('/planets', planetsRoutes);
server.use('/films', filmsRoutes);
server.use('/species', speciesRoutes);
server.use('/vehicles', vehiclesRoutes);
server.use('/starships', starshipsRoutes);

server.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});

server.use((err, req, res, next) => {
    return res.status(err.status || 500).render('error', {
        message: err.message || 'Unexpected error',
        status: err.status || 500,
    });
});

server.use('/', router);
server.listen(PORT,() => console.log(`Server started on http://localhost/${PORT}`));
