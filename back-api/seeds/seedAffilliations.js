const mongoose = require('mongoose');
const DB_URL = require('../db');

const Affiliations = require('../models/Affiliations');

const affiliationsData =[
    {
        id: 1,
        name: 'Jedi order',
        img:[]
    },
    {
        id: 2,
        name: 'Rebel alliance',
        img:[]
    },
    {
        id: 3,
        name: 'Galatic empire',
        img:[]
    }
]

mongoose.connect(DB_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(async ()=> {
        await Affiliations.create(affiliationsData);
    })
    .catch((err)=>{
        console.log(err.message);
    });