const mongoose = require('mongoose');
const DB_URL = require('../db');
const bcrypt = require('bcrypt');

const User = require('../models/Users');


const userDemo = {
    email: 'jonay.hdez@gmail.com',
    password: '123'
};

     mongoose.connect(DB_URL,{
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
        .then(async ()=>{
            const hash = await bcrypt.hash(userDemo.password, 10);
            const newUser = new User({
                email: userDemo.email.toLowerCase(),
                password: hash,
            });
           await newUser.save();
        })
        .catch((err)=>{
            console.log(err.message);
        })
