const mongoose = require('mongoose');
const DB_URL = require('../db');

const People = require('../models/People');
const Planets = require('../models/Planets');
const Films = require('../models/Films');
const Vehicles = require('../models/Vehicles');
const Species = require('../models/Species');
const Starships = require('../models/Starships');

const peopleImages = require('./imageSource/people');
const planetsImages = require('./imageSource/planets');
const filmsImages = require('./imageSource/films');
const vehiclesImages = require('./imageSource/vehicles');
const speciesImages = require('./imageSource/species');
const starshipsImages = require('./imageSource/starships');

const models = {
    'people': People,
    'planets': Planets
};

const postImages = async (array, model) => {
    const currentModel = models[model]
    for (const e of array) {
        await currentModel.findOneAndUpdate(
            { id:e.id },
            { img: e.img },
            { useFindAndModify: false }
        );
    }
    console.log(`Imagenes de ${model} cargadas`);
}

const images = ()=>{
    return mongoose.connect(DB_URL,{
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
        .then(async ()=>{
            await postImages(peopleImages, 'people');

            // for (const pImg of peopleImages) {
            //     await People.findOneAndUpdate(
            //         {id:pImg.id},
            //         { img: pImg.img },
            //         { useFindAndModify: false }
            //     );
            // }
            console.log("Imagenes de personajes cargadas");

            for (const pImg of planetsImages) {
                await Planets.findOneAndUpdate(
                    {id:pImg.id},
                    { img: pImg.img },
                    { useFindAndModify: false }
                );
            }
            console.log("Imagenes de planetas cargadas");

            for (const fImg of filmsImages) {
                await Films.findOneAndUpdate(
                    {id:fImg.id},
                    { img: fImg.img },
                    { useFindAndModify: false }
                );
            }
            console.log("Imagenes de peliculas cargadas");

            for (const vImg of vehiclesImages) {
                await Vehicles.findOneAndUpdate(
                    {id:vImg.id},
                    { img: vImg.img },
                    { useFindAndModify: false }
                );
            }
            console.log("Imagenes de vehiculos cargadas");

            for (const sImg of speciesImages) {
                await Species.findOneAndUpdate(
                    {id:sImg.id},
                    { img: sImg.img },
                    { useFindAndModify: false }
                );
            }
            console.log("Imagenes de especies cargadas");

            for (const sImg of starshipsImages) {
                await Starships.findOneAndUpdate(
                    {id:sImg.id},
                    { img: sImg.img },
                    { useFindAndModify: false }
                );
            }
            console.log("Imagenes de starships cargadas");

        })
        .catch((err)=>{
            console.log(err.message);
        })
}

module.exports = images;


