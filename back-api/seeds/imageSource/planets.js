const planetsImages =[
    {
        id: 1,
        img: ["https://img.unocero.com/2020/01/nasa-descubre-planeta-tatooine.jpg"]
    },
    {
        id: 2,
        img: ["https://i.ytimg.com/vi/EB_q_2eeZbI/maxresdefault.jpg"]
    },
    {
        id: 3,
        img: ["https://i.pinimg.com/originals/b6/c6/f1/b6c6f13855b066d132f5b7ab9cd12224.jpg"]
    },
    {
        id: 4,
        img: ["https://lumiere-a.akamaihd.net/v1/images/Hoth_d074d307.jpeg?region=0%2C38%2C1200%2C600"]
    },
    {
        id: 5,
        img: ["https://i.pinimg.com/originals/62/c1/bd/62c1bd67d39cdcf4d507f16b35f90f4f.jpg"]
    },
    {
        id: 6,
        img: ["https://static3.srcdn.com/wordpress/wp-content/uploads/2020/02/Bespin-Feature-Image-1.jpg"]
    },
    {
        id: 7,
        img: ["https://pm1.narvii.com/6470/b280692631df89cbf4bea8ef8f741b535dbd6f63_hq.jpg"]
    },
    {
        id: 8,
        img: ["https://i.pinimg.com/originals/ac/dd/1c/acdd1cd77aca5a5c784cca933a1bb229.jpg"]
    },
    {
        id: 9,
        img: ["https://pm1.narvii.com/6055/57332e64f8f9a0f092427045e0c703490cfee237_hq.jpg"]
    },
    {
        id: 10,
        img: ["https://starwarsblog.starwars.com/wp-content/uploads/2015/09/Image-00-Header-1536x864-688067241292.jpg"]
    }
];

module.exports = planetsImages;