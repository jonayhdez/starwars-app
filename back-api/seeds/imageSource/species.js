const speciesImages =[
    {
        id: 1,
        img: "https://static.wikia.nocookie.net/starwars/images/3/3f/HumansInTheResistance-TROS.jpg"
    },
    {
        id: 2,
        img: "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F6%2F2019%2F11%2Fbattle-droids-2000.jpg"
    },
    {
        id: 3,
        img: "https://img.cinemablend.com/filter:scale/quill/e/8/e/8/e/0/e8e8e0643843a5076caef010f47e521887f4c5f2.jpg?mw=600"
    },
    {
        id: 4,
        img: "https://static.wikia.nocookie.net/muc/images/8/84/Rodian.jpg"
    },
    {
        id:5,
        img:"https://i0.wp.com/super-ficcion.com/wp-content/uploads/2020/05/jabba-hutt_2.jpg"
    },
    {
        id:6,
        img:"https://img1.looper.com/img/gallery/everything-we-know-about-yodas-species/intro-1574290530.jpg"
    },
    {
        id:7,
        img:"https://starwarsblog.starwars.com/wp-content/uploads/2018/04/Bossk-header.jpg"
    },
    {
        id:8,
        img:"https://img1.looper.com/img/gallery/star-wars-the-mon-calamari-explained/intro-1605299609.jpg"
    },
    {
        id:9,
        img:"https://upload.wikimedia.org/wikipedia/en/e/ee/Wicket_W_Warrick.png"
    },
    {
        id:10,
        img:"https://pbs.twimg.com/media/DxhCvm4U0AYGr3c.png"
    }
];

module.exports = speciesImages;