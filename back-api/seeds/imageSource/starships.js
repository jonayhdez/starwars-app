const starshipsImages = [
    {
        id: 2,
        img: "https://static.wikia.nocookie.net/starwars/images/1/1c/Liberator-HSCK.png"
    },
    {
        id: 3,
        img: "https://i.etsystatic.com/8461382/r/il/395b99/1021787906/il_570xN.1021787906_hyhw.jpg"
    },
    {
        id: 5,
        img: "https://cdna.artstation.com/p/marketplace/presentation_assets/000/357/384/large/file.jpg"
    },
    {
        id:9,
        img: "https://lumiere-a.akamaihd.net/v1/images/Death-Star-I-copy_36ad2500.jpeg"
    },
    {
        id:10,
        img:"https://cnet3.cbsistatic.com/img/AvDLMTSs7B_kb3HedDZrSFrrMro=/940x528/2018/05/04/b7c5e76a-5c68-4fbd-8f68-72e2f7fe0293/millennium-falcon-018ea796.jpg"
    },
    {
        id:11,
        img: "https://lumiere-a.akamaihd.net/v1/images/Y-Wing-Fighter_0e78c9ae.jpeg"
    },
    {
        id:12,
        img: "https://image.invaluable.com/housePhotos/profilesinhistory/87/589387/H3257-L96343217.jpg"
    }
];

module.exports = starshipsImages;