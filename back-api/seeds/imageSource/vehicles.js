const vehiclesImages = [
    {
        id: 4,
        img: "https://upload.wikimedia.org/wikipedia/en/a/aa/Star_Wars_Sandcrawler.png"
    },
    {
        id: 6,
        img: "https://static.wikia.nocookie.net/esstarwars/images/f/f2/T-16_skyhopper_-_SW_20.png"
    },
    {
        id: 7,
        img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoxM5mOwXtausKgTjOYT18KkCSq6q11i91MA&usqp=CAU"
    },
    {
        id: 8,
        img: "https://pm1.narvii.com/6536/93173f45ef6e137cc562293a9c3d97f5c11c48ff_00.jpg"
    },
    {
        id: 14,
        img: "https://images.squarespace-cdn.com/content/v1/52615e54e4b051d1915a8890/1542053752891-6GSQDO9S1RY78MOYCR90/ke17ZwdGBToddI8pDm48kHPG90e6v4mF5R505MEkgZlZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpz9nmPiThqgW6vQv0I5rxv2uK9qgW72FcrSYZx_tX5F1y80y_FQ62Esv-KKE2WJKBo/Snowspeeder-600.png"
    },
    {
        id: 16,
        img: "https://static.wikia.nocookie.net/starwars/images/1/17/TIE_Bomber_BF2.png"
    },
    {
        id: 18,
        img: "https://lumiere-a.akamaihd.net/v1/images/AT-AT_89d0105f.jpeg?region=138%2C19%2C1392%2C697"
    },
    {
        id: 19,
        img: "https://as.com/betech/imagenes/2018/11/05/portada/1541439837_590159_1541447660_noticia_normal.jpg"
    },
    {
        id: 20,
        img: "https://static.wikia.nocookie.net/starwars/images/3/3b/Cloud-car-v2.png"
    },
    {
        id: 24,
        img:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjzliRTU6-1a56il8DeoHFRH-UyQW76_ajqg&usqp=CAU"
    }
];

module.exports = vehiclesImages;