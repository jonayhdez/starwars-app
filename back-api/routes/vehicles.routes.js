const express = require('express');
const vehiclesController = require('../controllers/vehicles.controller');
const router = express.Router();

router.get('/', vehiclesController.getAllVehicles);
router.get('/:id', vehiclesController.getVehicleById);
router.get('/page/:id', vehiclesController.getVehiclesByPage);

module.exports = router;