const express = require('express');
const filmsController = require('../controllers/films.controller');
const router = express.Router();

router.get('/',filmsController.getAllFilms);
router.get('/:id',filmsController.getFilmById);
router.get('/page/:id',filmsController.getFilmsByPage);

module.exports = router;