const express = require('express');
const speciesController = require('../controllers/species.controller');
const router = express.Router();

router.get('/', speciesController.getAllSpecies);
router.get('/:id', speciesController.getSpecieById);
router.get('/page/:id', speciesController.getSpeciesByPage);

module.exports = router;