const express = require('express');
const starshipsController = require('../controllers/starships.controller');
const router = express.Router();

router.get('/', starshipsController.getAllStarships);
router.get('/:id', starshipsController.getStarshipById);
router.get('/page/:id', starshipsController.getStarshipsByPage);

module.exports = router;