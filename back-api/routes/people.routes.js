const express = require('express');
const peopleController = require('../controllers/people.controller');
const router = express.Router();

router.get('/',peopleController.getAllChars);
router.get('/:id', peopleController.getCharsById);
router.get('/page/:id',peopleController.getCharsByPage);

router.put('/:id', peopleController.updateChar);

module.exports = router;
