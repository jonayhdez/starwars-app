const express = require('express');
const planetsController = require('../controllers/planets.controller');
const router = express.Router();

router.get('/', planetsController.getAllPlanets);
router.get('/:id', planetsController.getPlanetById);
router.get('/page/:id', planetsController.getPlanetsByPage);

module.exports = router;
