const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const User = require('./models/Users');
const saltRounds = 10;

passport.use(
    'login',
    new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async (req, email, password, done) => {
            try {
                const currentUser = await User.findOne({ email: email });

                if (!currentUser) {
                    const error = new Error('The user does not exist!');
                    return done(error);
                }

                console.log(currentUser);

                const isValidPassword = await bcrypt.compare(
                    password,
                    currentUser.password
                );

                if (!isValidPassword) {
                    const error = new Error(
                        'The email & password combination is incorrect!'
                    );
                    return done(error);
                }

                done(null, currentUser);
            } catch (err) {
                return done(err);
            }
        }
    )
);