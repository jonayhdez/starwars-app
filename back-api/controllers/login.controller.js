const passport = require('passport');

const getLogin = (req, res) => {
    return res.render('login');
};

const login = (req, res) => {
    passport.authenticate('login',null, (error, user) => {
        // if(error) {
        //     return res.render('login', { error: error.message });
        // }
        req.logIn(user, (error) => {
            // if(error) {
            //     return res.render('login', { error: error.message });
            // }
            return res.redirect('/home');
        });
    })(req, res);
}


module.exports = { getLogin, login };