const Planets = require('../models/Planets');

const getAllPlanets = (req, res, next) =>{
    return Planets.find().sort({id: 'asc'})
        .then(planets =>{
            const result={ result: planets }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

const getPlanetById = (req, res, next)=>{
    const id= req.params.id;

    return  Planets.find({id:id})
        .then(planet =>{
            return res.status(200).json(planet[0]);
        })
        .catch(err => {
            next(err);
        })
};

const getPlanetsByPage = (req, res, next)=>{
    const page = Math.max(0,req.params.id);
    const perPage = 10
    const skip = perPage * page - perPage

    return Planets.find()
        .skip(skip)
        .limit(perPage)
        .sort({id: 'asc'})
        .then(planets =>{
            const result={ result: planets }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })

};

module.exports = { getAllPlanets, getPlanetById, getPlanetsByPage };