const Films = require('../models/Films');

const getAllFilms = (req, res, next) =>{
    return Films.find().sort({id: 'asc'})
        .then(films =>{
            const result={ result: films }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

const getFilmById = (req, res, next)=>{
    const id= req.params.id;

    return  Films.find({id:id})
        .then(film =>{
            return res.status(200).json(film[0]);
        })
        .catch(err => {
            next(err);
        })
};

const getFilmsByPage = (req, res, next)=>{
    const page = Math.max(0,req.params.id);
    const perPage = 10
    const skip = perPage * page - perPage

    return Films.find()
        .skip(skip)
        .limit(perPage)
        .sort({id: 'asc'})
        .then(films =>{
            const result={ result: films }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

module.exports = { getAllFilms, getFilmById, getFilmsByPage };