const Vehicles = require('../models/Vehicles');

const getAllVehicles = (req, res, next) =>{
    return Vehicles.find().sort({id: 'asc'})
        .then(vehicles =>{
            const result={ result: vehicles }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

const getVehicleById = (req, res, next)=>{
    const id= req.params.id;

    return  Vehicles.find({id:id})
        .then(vehicle =>{
            return res.status(200).json(vehicle[0]);
        })
        .catch(err => {
            next(err);
        })
};

const getVehiclesByPage = (req, res, next)=>{
    const page = Math.max(0,req.params.id);
    const perPage = 10
    const skip = perPage * page - perPage

    return Vehicles.find()
        .skip(skip)
        .limit(perPage)
        .sort({id: 'asc'})
        .then(vehicles =>{
            const result={ result: vehicles }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

module.exports = { getAllVehicles, getVehicleById, getVehiclesByPage };