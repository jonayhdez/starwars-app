const Starships = require('../models/Starships');

const getAllStarships = (req, res, next) =>{
    return Starships.find().sort({id: 'asc'})
        .then(starships =>{
            const result={ result: starships }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

const getStarshipById = (req, res, next)=>{
    const id= req.params.id;

    return  Starships.find({id:id})
        .then(starship =>{
            return res.status(200).json(starship[0]);
        })
        .catch(err => {
            next(err);
        })
};

const getStarshipsByPage = (req, res, next)=>{
    const page = Math.max(0,req.params.id);
    const perPage = 10
    const skip = perPage * page - perPage

    return Starships.find()
        .skip(skip)
        .limit(perPage)
        .sort({id: 'asc'})
        .then(starships =>{
            const result={ result: starships }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

module.exports = { getAllStarships, getStarshipById, getStarshipsByPage };