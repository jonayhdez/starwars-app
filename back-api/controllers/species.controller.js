const Species = require('../models/Species');

const getAllSpecies = (req, res, next) =>{
    return Species.find().sort({id: 'asc'})
        .then(species =>{
            const result={ result: species }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

const getSpecieById = (req, res, next)=>{
    const id= req.params.id;

    return  Species.find({id:id})
        .then(specie =>{
            return res.status(200).json(specie[0]);
        })
        .catch(err => {
            next(err);
        })
};

const getSpeciesByPage = (req, res, next)=>{
    const page = Math.max(0,req.params.id);
    const perPage = 10
    const skip = perPage * page - perPage

    return Species.find()
        .skip(skip)
        .limit(perPage)
        .sort({id: 'asc'})
        .then(species =>{
            const result={ result: species }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

module.exports = { getAllSpecies, getSpecieById, getSpeciesByPage };
