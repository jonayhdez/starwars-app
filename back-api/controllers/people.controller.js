const People = require('../models/People');

const getAllChars = (req,res,next) =>{
    return People.find().sort({id: 'asc'})
        .then(chars =>{
            const result={ result: chars }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

const getCharsById = (req,res, next)=>{
    const id= req.params.id;

    return  People.find({id:id})
        .then(char =>{
            return res.status(200).json(char[0]);
        })
        .catch(err => {
            next(err);
        })
};

const getCharsByPage = (req, res, next)=>{
    const page = Math.max(0,req.params.id);
    const perPage = 10
    const skip = perPage * page - perPage

    return People.find()
        .skip(skip)
        .limit(perPage)
        .sort({id: 'asc'})
        .then(chars =>{
            const result={ result: chars }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);;
        })
};

const updateChar = async (req, res, next) => {
    try {
        const id = req.body._id;
        const idParam = req.params.id;

        const updateChar = await People.findByIdAndUpdate(
            id,
            { img: req.body.img },
            { new: true }
        );

        return res.status(200).json(updateChar);
    } catch (err) {
        next(err);
    }
};

module.exports = { getAllChars, getCharsById, getCharsByPage, updateChar };