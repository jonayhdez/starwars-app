const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const peopleSchema = new Schema(
    {
        id:{
            type: Number,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        img:{
            type: Array,
            required: true
        },
        height: {
            type: String,
            required: true
        },
        mass: {
            type: String,
            required: true
        },
        hair_color:{
            type: String,
            required: true
        },
        skin_color:{
            type: String,
            required: true
        },
        eye_color:{
            type: String,
            required: true
        },
        birth_year:{
            type: String,
            required: true
        },
        gender:{
            type: String,
            required: true
        },
        homeworld:{
            type: Array,
            required: true
        },
        films:{
            type: Array,
            required: true
        },
        species:{
            type: Array,
            required: true
        },
        vehicles:{
            type: Array,
            required: true
        },
        starships:{
            type: Array,
            required: true
        },
        affiliate:{
            type: Array
        }
    },
    {
        timestamps:true,
    }
);

const People = mongoose.model('People', peopleSchema);
module.exports = People;