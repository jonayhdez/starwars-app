const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const planetSchema = new Schema(
    {
        id:{
            type: Number,
            required: true
        },
        name:{
            type: String,
            required: true
        },
        img:{
            type: Array,
            required: true
        },
        rotation_period:{
            type: String,
            required: true
        },
        orbital_period:{
            type: String,
            required: true
        },
        diameter:{
            type: String,
            required: true
        },
        climate:{
            type: String,
            required: true
        },
        gravity:{
            type: String,
            required: true
        },
        terrain:{
            type: String,
            required: true
        },
        surface_water:{
            type: String,
            required: true
        },
        population:{
            type: String,
            required: true
        },
        residents:{
            type: Array,
            required: true
        },
        films:{
            type: Array,
            required: true
        }
    },
    {
        timestamps:true,
    }
);

const Planets = mongoose.model('Planets', planetSchema);
module.exports = Planets;