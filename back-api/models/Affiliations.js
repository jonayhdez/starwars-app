const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const affiliateSchema = new Schema(
    {
        id: {
            type: Number,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        img: {
            type: Array
        }
    },
    {
        timestamps:true,
    }
);

const Affiliations = mongoose.model('Affiliations', affiliateSchema);
module.exports = Affiliations;