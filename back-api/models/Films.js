const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const filmsSchema = new Schema(
    {
        id:{
            type: Number,
            required: true
        },
        img:{
            type: Array,
            required: true
        },
        title:{
            type: String,
            required: true
        },
        episode_id:{
            type: Number,
            required: true
        },
        opening_crawl:{
            type: String,
            required: true
        },
        director:{
            type: String,
            required: true
        },
        producer:{
            type: String,
            required: true
        },
        release_date:{
            type: String,
            required: true
        },
        characters:{
            type: Array,
            required: true
        },
        planets:{
            type: Array,
            required: true
        },
        starships:{
            type: Array,
            required: true
        },
        vehicles:{
            type: Array,
            required: true
        },
        species:{
            type: Array,
            required: true
        }
    },
    {
        timestamps:true
    }
);

const Films = mongoose.model('Films', filmsSchema);
module.exports = Films;