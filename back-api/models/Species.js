const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const specieSchema = new Schema(
    {
        id:{
            type: Number,
            required: true
        },
        name:{
            type: String,
            required: true
        },
        img:{
            type: Array,
            required: true
        },
        classification:{
            type: String,
            required: true
        },
        designation:{
            type: String,
            required: true
        },
        average_height:{
            type: String,
            required: true
        },
        skin_colors:{
            type: String,
            required: true
        },
        hair_colors:{
            type: String,
            required: true
        },
        eye_colors:{
            type: String,
            required: true
        },
        average_lifespan:{
            type: String,
            required: true
        },
        homeworld:{
            type: String
        },
        language:{
            type: String,
            required: true
        },
        people:{
            type: Array,
            required: true
        },
        films:{
            type: Array,
            required: true
        }
    },
    {
        timestamps:true
    }
);

const Species = mongoose.model('Species', specieSchema);
module.exports = Species;