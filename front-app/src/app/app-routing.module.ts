import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo:'home', pathMatch:'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then((m)=>m.HomeModule),
  },
  {
    path: 'characters',
    loadChildren: () => import('./pages/characters/characters.module').then((m)=>m.CharactersModule),
  },
  {
    path: 'characters/:id',
    loadChildren: () => import('./pages/characters/detail/detail.module').then((m)=>m.DetailModule)
  },
  {
    path: 'planets',
    loadChildren: () => import('./pages/planets/planets.module').then((m)=>m.PlanetsModule),
  },
  {
    path: 'films',
    loadChildren: () => import('./pages/films/films.module').then((m)=>m.FilmsModule),
  },
  {
    path: 'species',
    loadChildren: () => import('./pages/species/species.module').then((m)=>m.SpeciesModule),
  },
  {
    path: 'vehicles',
    loadChildren: () => import('./pages/vehicles/vehicles.module').then((m)=>m.VehiclesModule),
  },
  {
    path: 'starships',
    loadChildren: () => import('./pages/starships/starships.module').then((m)=>m.StarshipsModule),
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then((m)=>m.LoginModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
