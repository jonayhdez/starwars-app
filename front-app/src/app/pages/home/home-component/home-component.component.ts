import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.scss']
})
export class HomeComponent implements OnInit {

  public slides = [
    'https://fondosmil.com/fondo/23629.jpg',
    'https://free4kwallpapers.com/uploads/originals/2020/09/19/star-wars-the-mandalorian-wallpaper.jpg',
    'https://fondosmil.com/fondo/23596.jpg',
  ];
  constructor() {}

  ngOnInit(): void {
  }

}
