import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SWSpecie} from "../../../../models/models-interfaces";

@Component({
  selector: 'app-species-list',
  templateUrl: './species-list.component.html',
  styleUrls: ['./species-list.component.scss']
})
export class SpeciesListComponent implements OnInit {
  @Input() species: SWSpecie[] | any;
  @Output() emitMessage = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onScroll(e:boolean){
    this.emitMessage.emit(e);
  }
}
