import { Component, OnInit } from '@angular/core';
import {SWSpecie} from "../../../models/models-interfaces";
import {MySwapiService} from "../../../services/mySwapi/my-swapi.service";
import {NgxSpinnerService} from "ngx-spinner";
import { faFilter, faTh, faSortAlphaDown, faSortAlphaUp, faSearch } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-species-component',
  templateUrl: './species-component.component.html',
  styleUrls: ['./species-component.component.scss']
})
export class SpeciesComponent implements OnInit {
  species: SWSpecie[] = [];
  speciesCopy: SWSpecie[]=this.species;
  speciesOriginal: SWSpecie[]=this.species;
  page:number=1;
  filterName=false;
  filterNameOrder:boolean | null=null;
  notEmptyPost = true;
  notScrolly = false;
  filter: string = '';

  faFilter = faFilter;
  faTh=faTh;
  faSortAlphaDown = faSortAlphaDown;
  faSortAlphaUp=faSortAlphaUp;
  faSearch = faSearch;

  constructor(private mySwapiService:MySwapiService, private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    this.loadFirstPage();
  }
  onChangeFilter(filter: string) {
    this.species = [...this.speciesCopy];
    this.species =  this.species.filter(el => el.name.toLowerCase().includes(filter.trim().toLowerCase()));;
  }

  loadFirstPage(){
    this.mySwapiService.getDataPage('species',this.page).subscribe(
      (result: [] | any) => {
        this.species=result;
        this.speciesCopy = [...this.species];
        this.speciesOriginal = [...this.species];
        if (result.length===0){
          this.notEmptyPost=false
        }else {
          ++this.page;
        }
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  onScroll(e:boolean){
    if (e && this.notEmptyPost) {
      this.spinner.show();
      this.notScrolly = false;
      setTimeout (() => {
        this.loadNextPage();
      }, 1000);
    }
  }

  loadNextPage(){
    if (this.notEmptyPost) {
      this.mySwapiService.getDataPage('species', this.page).subscribe(
        (result: [] | any) => {
          this.species = [...this.species,...result];
          this.speciesCopy = [...this.species];
          this.speciesOriginal = [...this.species];
          this.spinner.hide();

          if (result.length<10){
            this.notEmptyPost=false
          }else{
            ++this.page;
          }
          this.notScrolly = true;
        },
        (err) => {
          this.notEmptyPost = false;
          console.error(err.message);
        }
      );
    }
  }

  public orderByName(){
    if (this.filterNameOrder === false || this.filterNameOrder=== null){
      this.species.sort((a:SWSpecie, b:SWSpecie)=> {
        this.filterName=true;
        this.filterNameOrder=true;
        if (a.name > b.name) {
          return 1;
        } else if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
      this.speciesCopy = [...this.species];
    }else{
      this.removeOrderByName();
    }
  }

  public orderByNameInverse(){
    if (this.filterNameOrder === true || this.filterNameOrder=== null){
      this.species.sort((a:SWSpecie, b:SWSpecie)=> {
        this.filterName=true;
        this.filterNameOrder=false;
        if (a.name > b.name) {
          return -1;
        } else if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
      this.speciesCopy = [...this.species];
    }else{
      this.removeOrderByName();
    }
  }

  public removeOrderByName(){
    this.filterName=false;
    this.filterNameOrder=null;
    this.species= [...this.speciesOriginal];
    this.speciesCopy= [...this.speciesOriginal];

  }

}
