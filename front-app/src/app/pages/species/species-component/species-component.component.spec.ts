import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeciesComponentComponent } from './species-component.component';

describe('SpeciesComponentComponent', () => {
  let component: SpeciesComponentComponent;
  let fixture: ComponentFixture<SpeciesComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpeciesComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeciesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
