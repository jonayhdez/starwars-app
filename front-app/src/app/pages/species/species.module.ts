import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpeciesRoutingModule } from './species-routing.module';
import { SpeciesComponent } from './species-component/species-component.component';
import { SpeciesListComponent } from './species-component/species-list/species-list.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {MatSidenavModule} from "@angular/material/sidenav";
import {NgxSpinnerModule} from "ngx-spinner";
import {InfiniteScrollModule} from "ngx-infinite-scroll";


@NgModule({
  declarations: [SpeciesComponent, SpeciesListComponent],
  imports: [
    CommonModule,
    SpeciesRoutingModule,
    FontAwesomeModule,
    FormsModule,
    MatSidenavModule,
    NgxSpinnerModule,
    InfiniteScrollModule
  ]
})
export class SpeciesModule { }
