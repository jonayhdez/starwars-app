import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  public loginForm: FormGroup | any;
  public submitted: boolean = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createLoginForm();
  }

  public createLoginForm(): void{
    this.loginForm=this.formBuilder.group({
      email: ['',[Validators.required]],
      password: ['',[Validators.required]],
    });
  }

  public onSubmit():void {
    this.submitted=true;
    if (this.loginForm.valid){
      this.loginForm.reset();
      this.submitted=false;
    }
  }

}
