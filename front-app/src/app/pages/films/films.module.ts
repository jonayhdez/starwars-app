import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilmsRoutingModule } from './films-routing.module';
import { FilmsComponent } from './films-component/films-component.component';
import { FilmsListComponent } from './films-component/films-list/films-list.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {MatSidenavModule} from "@angular/material/sidenav";
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {NgxSpinnerModule} from "ngx-spinner";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [FilmsComponent, FilmsListComponent],
  imports: [
    CommonModule,
    FilmsRoutingModule,
    FontAwesomeModule,
    FormsModule,
    MatSidenavModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    TranslateModule
  ]
})
export class FilmsModule { }
