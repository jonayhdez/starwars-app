import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SWFilm} from "../../../../models/models-interfaces";

@Component({
  selector: 'app-films-list',
  templateUrl: './films-list.component.html',
  styleUrls: ['./films-list.component.scss']
})
export class FilmsListComponent implements OnInit {
  @Input() films: SWFilm[] | any;
  @Output() emitMessage = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onScroll(e:boolean){
    this.emitMessage.emit(e);
  }
}
