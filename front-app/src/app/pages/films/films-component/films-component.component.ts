import { Component, OnInit } from '@angular/core';
import { SWFilm } from "../../../models/models-interfaces";
import {MySwapiService} from "../../../services/mySwapi/my-swapi.service";
import {NgxSpinnerService} from "ngx-spinner";
import { faFilter, faTh, faSortAlphaDown, faSortAlphaUp, faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-films-component',
  templateUrl: './films-component.component.html',
  styleUrls: ['./films-component.component.scss']
})
export class FilmsComponent implements OnInit {
  films: SWFilm[] = [];
  filmsCopy: SWFilm[]=this.films;
  filmsOriginal: SWFilm[]=this.films;
  page:number=1;
  filterName=false;
  filterNameOrder:boolean | null=null;
  notEmptyPost = true;
  notScrolly = false;
  filter: string = '';

  faFilter = faFilter;
  faTh=faTh;
  faSortAlphaDown = faSortAlphaDown;
  faSortAlphaUp=faSortAlphaUp;
  faSearch = faSearch;

  constructor(private mySwapiService:MySwapiService, private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    this.loadFirstPage();
  }
  onChangeFilter(filter: string) {
    this.films = [...this.filmsCopy];
    this.films =  this.films.filter(el => el.title.toLowerCase().includes(filter.trim().toLowerCase()));;
  }

  loadFirstPage(){
    this.mySwapiService.getDataPage('films',this.page).subscribe(
      (result: [] | any) => {
        this.films=result;
        this.filmsCopy = [...this.films];
        this.filmsOriginal = [...this.films];
        if (result.length===0){
          this.notEmptyPost=false
        }else {
          ++this.page;
        }
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  onScroll(e:boolean){
    if (e && this.notEmptyPost) {
      this.spinner.show();
      this.notScrolly = false;
      setTimeout (() => {
        this.loadNextPage();
      }, 1000);
    }
  }

  loadNextPage(){
    if (this.notEmptyPost) {
      this.mySwapiService.getDataPage('films', this.page).subscribe(
        (result: [] | any) => {
          this.films = [...this.films,...result];
          this.filmsCopy = [...this.films];
          this.filmsOriginal = [...this.films];
          this.spinner.hide();

          if (result.length<10){
            this.notEmptyPost=false
          }else{
            ++this.page;
          }
          this.notScrolly = true;
        },
        (err) => {
          this.notEmptyPost = false;
          console.error(err.message);
        }
      );
    }
  }

  public orderByName(){
    if (this.filterNameOrder === false || this.filterNameOrder=== null){
      this.films.sort((a:SWFilm, b:SWFilm)=> {
        this.filterName=true;
        this.filterNameOrder=true;
        if (a.title > b.title) {
          return 1;
        } else if (a.title < b.title) {
          return -1;
        }
        return 0;
      });
      this.filmsCopy = [...this.films];
    }else{
      this.removeOrderByName();
    }
  }

  public orderByNameInverse(){
    if (this.filterNameOrder === true || this.filterNameOrder=== null){
      this.films.sort((a:SWFilm, b:SWFilm)=> {
        this.filterName=true;
        this.filterNameOrder=false;
        if (a.title > b.title) {
          return -1;
        } else if (a.title < b.title) {
          return 1;
        }
        return 0;
      });
      this.filmsCopy = [...this.films];
    }else{
      this.removeOrderByName();
    }
  }

  public removeOrderByName(){
    this.filterName=false;
    this.filterNameOrder=null;
    this.films= [...this.filmsOriginal];
    this.filmsCopy= [...this.filmsOriginal];

  }

}
