import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StarshipsRoutingModule } from './starships-routing.module';
import { StarshipsComponent } from './starships-component/starships-component.component';
import { StarshipsListComponent } from './starships-component/starships-list/starships-list.component';
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {NgxSpinnerModule} from "ngx-spinner";
import {MatSidenavModule} from "@angular/material/sidenav";
import {FormsModule} from "@angular/forms";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";


@NgModule({
  declarations: [StarshipsComponent, StarshipsListComponent],
  imports: [
    CommonModule,
    StarshipsRoutingModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    MatSidenavModule,
    FormsModule,
    FontAwesomeModule
  ]
})
export class StarshipsModule { }
