import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StarshipsComponent} from "./starships-component/starships-component.component";

const routes: Routes = [{ path: '', component: StarshipsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StarshipsRoutingModule { }
