import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StarshipsComponentComponent } from './starships-component.component';

describe('StarshipsComponentComponent', () => {
  let component: StarshipsComponentComponent;
  let fixture: ComponentFixture<StarshipsComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StarshipsComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StarshipsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
