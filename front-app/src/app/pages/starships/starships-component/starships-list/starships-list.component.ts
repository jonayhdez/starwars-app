import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { SWStarship} from "../../../../models/models-interfaces";

@Component({
  selector: 'app-starships-list',
  templateUrl: './starships-list.component.html',
  styleUrls: ['./starships-list.component.scss']
})
export class StarshipsListComponent implements OnInit {
  @Input() starships: SWStarship[] | any;
  @Output() emitMessage = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onScroll(e:boolean){
    this.emitMessage.emit(e);
  }
}
