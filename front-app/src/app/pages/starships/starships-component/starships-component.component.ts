import { Component, OnInit } from '@angular/core';
import {SWStarship} from "../../../models/models-interfaces";
import {MySwapiService} from "../../../services/mySwapi/my-swapi.service";
import {NgxSpinnerService} from "ngx-spinner";
import { faFilter, faTh, faSortAlphaDown, faSortAlphaUp, faSearch } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-starships-component',
  templateUrl: './starships-component.component.html',
  styleUrls: ['./starships-component.component.scss']
})
export class StarshipsComponent implements OnInit {
  starships: SWStarship[] = [];
  starshipsCopy: SWStarship[]=this.starships;
  starshipsOriginal: SWStarship[]=this.starships;
  page:number=1;
  filterName=false;
  filterNameOrder:boolean | null=null;
  notEmptyPost = true;
  notScrolly = false;
  filter: string = '';

  faFilter = faFilter;
  faTh=faTh;
  faSortAlphaDown = faSortAlphaDown;
  faSortAlphaUp=faSortAlphaUp;
  faSearch = faSearch;

  constructor(private mySwapiService:MySwapiService, private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    this.loadFirstPage();
  }
  onChangeFilter(filter: string) {
    this.starships = [...this.starshipsCopy];
    this.starships =  this.starships.filter(el => el.name.toLowerCase().includes(filter.trim().toLowerCase()));;
  }

  loadFirstPage(){
    this.mySwapiService.getDataPage('starships',this.page).subscribe(
      (result: [] | any) => {
        this.starships=result;
        this.starshipsCopy = [...this.starships];
        this.starshipsOriginal = [...this.starships];
        if (result.length===0){
          this.notEmptyPost=false
        }else {
          ++this.page;
        }
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  onScroll(e:boolean){
    if (e && this.notEmptyPost) {
      this.spinner.show();
      this.notScrolly = false;
      setTimeout (() => {
        this.loadNextPage();
      }, 1000);
    }
  }

  loadNextPage(){
    if (this.notEmptyPost) {
      this.mySwapiService.getDataPage('starships', this.page).subscribe(
        (result: [] | any) => {
          this.starships = [...this.starships,...result];
          this.starshipsCopy = [...this.starships];
          this.starshipsOriginal = [...this.starships];
          this.spinner.hide();

          if (result.length<10){
            this.notEmptyPost=false
          }else{
            ++this.page;
          }
          this.notScrolly = true;
        },
        (err) => {
          this.notEmptyPost = false;
          console.error(err.message);
        }
      );
    }
  }

  public orderByName(){
    if (this.filterNameOrder === false || this.filterNameOrder=== null){
      this.starships.sort((a:SWStarship, b:SWStarship)=> {
        this.filterName=true;
        this.filterNameOrder=true;
        if (a.name > b.name) {
          return 1;
        } else if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
      this.starshipsCopy = [...this.starships];
    }else{
      this.removeOrderByName();
    }
  }

  public orderByNameInverse(){
    if (this.filterNameOrder === true || this.filterNameOrder=== null){
      this.starships.sort((a:SWStarship, b:SWStarship)=> {
        this.filterName=true;
        this.filterNameOrder=false;
        if (a.name > b.name) {
          return -1;
        } else if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
      this.starshipsCopy = [...this.starships];
    }else{
      this.removeOrderByName();
    }
  }

  public removeOrderByName(){
    this.filterName=false;
    this.filterNameOrder=null;
    this.starships= [...this.starshipsOriginal];
    this.starshipsCopy= [...this.starshipsOriginal];

  }

}
