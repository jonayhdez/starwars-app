import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VehiclesRoutingModule } from './vehicles-routing.module';
import { VehiclesComponent } from './vehicles-component/vehicles-component.component';
import { VehiclesListComponent } from './vehicles-component/vehicles-list/vehicles-list.component';
import {NgxSpinnerModule} from "ngx-spinner";
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {MatSidenavModule} from "@angular/material/sidenav";


@NgModule({
  declarations: [VehiclesComponent, VehiclesListComponent],
  imports: [
    CommonModule,
    VehiclesRoutingModule,
    NgxSpinnerModule,
    InfiniteScrollModule,
    FontAwesomeModule,
    FormsModule,
    MatSidenavModule
  ]
})
export class VehiclesModule { }
