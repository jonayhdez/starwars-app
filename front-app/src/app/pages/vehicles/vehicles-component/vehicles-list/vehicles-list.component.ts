import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SWVehicle} from "../../../../models/models-interfaces";

@Component({
  selector: 'app-vehicles-list',
  templateUrl: './vehicles-list.component.html',
  styleUrls: ['./vehicles-list.component.scss']
})
export class VehiclesListComponent implements OnInit {
  @Input() vehicles: SWVehicle[] | any;
  @Output() emitMessage = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onScroll(e:boolean){
    this.emitMessage.emit(e);
  }
}
