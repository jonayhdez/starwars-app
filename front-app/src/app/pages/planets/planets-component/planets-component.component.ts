import { Component, OnInit } from '@angular/core';
import { SWPlanet } from "../../../models/models-interfaces";
import { MySwapiService } from "../../../services/mySwapi/my-swapi.service";
import { NgxSpinnerService } from "ngx-spinner";
import { faFilter, faTh, faSortAlphaDown, faSortAlphaUp, faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-planets-component',
  templateUrl: './planets-component.component.html',
  styleUrls: ['./planets-component.component.scss']
})
export class PlanetsComponent implements OnInit {
  planets: SWPlanet [] = [];
  planetsCopy: SWPlanet[]=this.planets;
  planetsOriginal: SWPlanet[]=this.planets;
  page:number=1;
  filterName=false;
  filterNameOrder:boolean | null=null;
  notEmptyPost = true;
  notScrolly = false;
  filter: string = '';

  faFilter = faFilter;
  faTh=faTh;
  faSortAlphaDown = faSortAlphaDown;
  faSortAlphaUp=faSortAlphaUp;
  faSearch = faSearch;

  constructor(private mySwapiService:MySwapiService, private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    this.loadFirstPage();
  }
  onChangeFilter(filter: string) {
    this.planets = [...this.planetsCopy];
    this.planets =  this.planets.filter(el => el.name.toLowerCase().includes(filter.trim().toLowerCase()));;
  }

  loadFirstPage(){
    this.mySwapiService.getDataPage('planets',this.page).subscribe(
      (result: [] | any) => {
        this.planets=result;
        this.planetsCopy = [...this.planets];
        this.planetsOriginal = [...this.planets];
        if (result.length===0){
          this.notEmptyPost=false
        }else {
          ++this.page;
        }
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  onScroll(e:boolean){
    if (e && this.notEmptyPost) {
      this.spinner.show();
      this.notScrolly = false;
      setTimeout (() => {
        this.loadNextPage();
      }, 1000);
    }
  }

  loadNextPage(){
    if (this.notEmptyPost) {
      this.mySwapiService.getDataPage('planets', this.page).subscribe(
        (result: [] | any) => {
          this.planets = [...this.planets,...result];
          this.planetsCopy = [...this.planets];
          this.planetsOriginal = [...this.planets];
          this.spinner.hide();

          if (result.length<10){
            this.notEmptyPost=false
          }else{
            ++this.page;
          }
          this.notScrolly = true;
        },
        (err) => {
          this.notEmptyPost = false;
          console.error(err.message);
        }
      );
    }
  }

  public orderByName(){
    if (this.filterNameOrder === false || this.filterNameOrder=== null){
      this.planets.sort((a:SWPlanet, b:SWPlanet)=> {
        this.filterName=true;
        this.filterNameOrder=true;
        if (a.name > b.name) {
          return 1;
        } else if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
      this.planetsCopy = [...this.planets];
    }else{
      this.removeOrderByName();
    }
  }

  public orderByNameInverse(){
    if (this.filterNameOrder === true || this.filterNameOrder=== null){
      this.planets.sort((a:SWPlanet, b:SWPlanet)=> {
        this.filterName=true;
        this.filterNameOrder=false;
        if (a.name > b.name) {
          return -1;
        } else if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
      this.planetsCopy = [...this.planets];
    }else{
      this.removeOrderByName();
    }
  }

  public removeOrderByName(){
    this.filterName=false;
    this.filterNameOrder=null;
    this.planets= [...this.planetsOriginal];
    this.planetsCopy= [...this.planetsOriginal];

  }

}
