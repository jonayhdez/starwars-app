import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetsComponentComponent } from './planets-component.component';

describe('PlanetsComponentComponent', () => {
  let component: PlanetsComponentComponent;
  let fixture: ComponentFixture<PlanetsComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanetsComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
