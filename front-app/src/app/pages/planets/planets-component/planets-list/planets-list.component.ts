import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { SWPlanet } from "../../../../models/models-interfaces";

@Component({
  selector: 'app-planets-list',
  templateUrl: './planets-list.component.html',
  styleUrls: ['./planets-list.component.scss']
})
export class PlanetsListComponent implements OnInit {
  @Input() planets: SWPlanet[] | any;
  @Output() emitMessage = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onScroll(e:boolean){
    this.emitMessage.emit(e);
  }
}
