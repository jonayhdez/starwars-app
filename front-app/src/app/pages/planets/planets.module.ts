import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanetsRoutingModule } from './planets-routing.module';
import { PlanetsComponent } from "./planets-component/planets-component.component";
import { PlanetsListComponent } from './planets-component/planets-list/planets-list.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSidenavModule} from "@angular/material/sidenav";
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {NgxSpinnerModule} from "ngx-spinner";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [PlanetsComponent, PlanetsListComponent],
    imports: [
        CommonModule,
        PlanetsRoutingModule,
        MatToolbarModule,
        MatSidenavModule,
        InfiniteScrollModule,
        NgxSpinnerModule,
        FontAwesomeModule,
        FormsModule,
        TranslateModule
    ]
})
export class PlanetsModule { }
