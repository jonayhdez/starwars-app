import { Component, OnInit } from '@angular/core';
import { SWPeople } from "../../../models/models-interfaces";
import { NgxSpinnerService } from "ngx-spinner";
import { MySwapiService } from "../../../services/mySwapi/my-swapi.service";
import { faFilter, faTh, faSortAlphaDown, faSortAlphaUp, faSearch } from '@fortawesome/free-solid-svg-icons';
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-character-component',
  templateUrl: './character-component.component.html',
  styleUrls: ['./character-component.component.scss']
})
export class CharacterComponent implements OnInit {
  characters: SWPeople[] = [];
  charactersCopy: SWPeople[]=this.characters;
  charactersOriginal: SWPeople[]=this.characters;
  page:number=1;
  filterName=false;
  filterNameOrder:boolean | null=null;
  notEmptyPost = true;
  notScrolly = false;
  filter: string = '';

  faFilter = faFilter;
  faTh=faTh;
  faSortAlphaDown = faSortAlphaDown;
  faSortAlphaUp=faSortAlphaUp;
  faSearch = faSearch;


  constructor(public translate: TranslateService, private mySwapiService:MySwapiService,private spinner:NgxSpinnerService) {


  }

  ngOnInit(): void{
    this.loadFirstPage();
  }

  onChangeFilter(filter: string) {
    this.characters = [...this.charactersCopy];
    this.characters =  this.characters.filter(el => el.name.toLowerCase().includes(filter.trim().toLowerCase()));;
  }

  loadFirstPage(){
    this.mySwapiService.getDataPage('people',this.page).subscribe(
      (result: [] | any) => {
        this.characters=result;
        this.charactersCopy = [...this.characters];
        this.charactersOriginal = [...this.characters];
        if (result.length===0){
          this.notEmptyPost=false
        }else {
          ++this.page;
        }
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  onScroll(e:boolean){
    if (e && this.notEmptyPost) {
      this.spinner.show();
      this.notScrolly = false;
      setTimeout (() => {
         this.loadNextPage();
      }, 1000);

    }
  }

  loadNextPage(){
    if (this.notEmptyPost) {
      this.mySwapiService.getDataPage('people', this.page).subscribe(
        (result: [] | any) => {
          this.characters = [...this.characters,...result];
          this.charactersCopy = [...this.characters];
          this.charactersOriginal = [...this.characters];
          this.checkFilters();
          this.spinner.hide();

          if (result.length<10){
            this.notEmptyPost=false
          }else{
            ++this.page;
          }

          this.notScrolly = true;
        },
        (err) => {
          this.notEmptyPost = false;
          console.error(err.message);
        }
      );
    }
  }

  public checkFilters(){
    if (this.filterName){
      this.orderByName();
    }
  }

  public orderByName(){
    if (this.filterNameOrder === false || this.filterNameOrder=== null){
      this.characters.sort((a:SWPeople, b:SWPeople)=> {
        this.filterName=true;
        this.filterNameOrder=true;
        if (a.name > b.name) {
          return 1;
        } else if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
      this.charactersCopy = [...this.characters];
    }else{
      this.removeOrderByName();
    }
  }

  public orderByNameInverse(){
    if (this.filterNameOrder === true || this.filterNameOrder=== null){
      this.characters.sort((a:SWPeople, b:SWPeople)=> {
        this.filterName=true;
        this.filterNameOrder=false;
        if (a.name > b.name) {
          return -1;
        } else if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
      this.charactersCopy = [...this.characters];
    }else{
      this.removeOrderByName();
    }
  }

  public removeOrderByName(){
    this.filterName=false;
    this.filterNameOrder=null;
    this.characters= [...this.charactersOriginal];
    this.charactersCopy= [...this.charactersOriginal];

  }
}

