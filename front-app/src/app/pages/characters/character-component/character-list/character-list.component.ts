import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { SWPeople } from "../../../../models/models-interfaces";

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit {
  @Input() chars: SWPeople[] | any;
  @Output() emitMessage = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  onScroll(e:boolean){
    this.emitMessage.emit(e);
  }
}
