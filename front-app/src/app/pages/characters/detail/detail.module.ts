import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailRoutingModule } from './detail-routing.module';
import { DetailComponent } from './detail-component/detail-component.component';
import { DetailCardComponent } from './detail-component/detail-card/detail-card.component';
import { ModalFormMyApiComponent } from './detail-component/detail-card/modal-form-my-api/modal-form-my-api.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [DetailComponent, DetailCardComponent, ModalFormMyApiComponent],
    imports: [
        CommonModule,
        DetailRoutingModule,
        ReactiveFormsModule
    ]
})
export class DetailModule { }
