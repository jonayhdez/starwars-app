import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import {MySwapiService} from "../../../../services/mySwapi/my-swapi.service";

@Component({
  selector: 'app-detail-component',
  templateUrl: './detail-component.component.html',
  styleUrls: ['./detail-component.component.scss']
})
export class DetailComponent implements OnInit {
public charId: string | any;
private endPoint:string='people';
public char: {} ={};
  constructor(private mySwapiService:MySwapiService, private location:Location, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getDetail();
  }

  getDetail(){
    this.route.paramMap.subscribe(param =>{
      this.charId=param.get('id');
    });

    this.mySwapiService.getDataId(this.endPoint,this.charId).subscribe(
      (result:any) => {
        this.char=result;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

}
