import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFormMyApiComponent } from './modal-form-my-api.component';

describe('ModalFormMyApiComponent', () => {
  let component: ModalFormMyApiComponent;
  let fixture: ComponentFixture<ModalFormMyApiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalFormMyApiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFormMyApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
