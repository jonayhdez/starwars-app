import {Component, Input, OnInit} from '@angular/core';
import { SWPeople} from "../../../../../models/models-interfaces";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {MySwapiService} from "../../../../../services/mySwapi/my-swapi.service";

@Component({
  selector: 'app-detail-card',
  templateUrl: './detail-card.component.html',
  styleUrls: ['./detail-card.component.scss']
})
export class DetailCardComponent implements OnInit {
  @Input() char:SWPeople | any;
  @Input() public showModal: boolean = false;
  private endPoint:string='people';
  public imgForm: FormGroup | any;
  public submitted: boolean = false;

  constructor(private mySwapiService:MySwapiService,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createMyApiForm();
  }

  showModalImage(){
    this.showModal=true;
  }

  putImage(){
    const newImg = {
      _id: this.char._id,
      img: [this.imgForm.get('url').value]
    };
    this.char.img=newImg.img;

    this.mySwapiService.putDataId(this.endPoint,this.char.id,newImg).subscribe(
      (result:any) => {
        return result
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public createMyApiForm(): void{
    this.imgForm=this.formBuilder.group({
      url: ['',[Validators.required]]
    });
  }

  public onSubmit():void {
    this.submitted=true;
    if (this.imgForm.valid){
      this.showModal=false;
      this.putImage();
      this.imgForm.reset();
      this.submitted=false;
    }
  }

}
