import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharactersRoutingModule } from './characters-routing.module';
import { CharacterComponent } from './character-component/character-component.component';
import { CharacterListComponent } from './character-component/character-list/character-list.component';
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import { NgxSpinnerModule } from "ngx-spinner";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [CharacterComponent, CharacterListComponent],
    imports: [
        CommonModule,
        CharactersRoutingModule,
        InfiniteScrollModule,
        NgxSpinnerModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        FontAwesomeModule,
        FormsModule,
        TranslateModule,
    ]
})
export class CharactersModule { }
