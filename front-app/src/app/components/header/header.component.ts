import { Component, OnInit } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
menuMobile = false;
language = 'en';

  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'es']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang);
  }

  ngOnInit(): void {
  }

  public menuHambur(){
    debugger
    this.menuMobile = !this.menuMobile;
  }

  public setLanguage(e: string){
    this.translate.use(e);
  }
}
