import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { GlobalVar } from "../../keys/keys";

@Injectable({
  providedIn: 'root'
})
export class MySwapiService {
  private mySwapiUrl = GlobalVar.mySwapiUrl;

  constructor(private http:HttpClient) { }

  public getData(endPoint:string): Observable<any>{
    return this.http.get(`${this.mySwapiUrl}/${endPoint}`).pipe(
      map((response:any) => {
        if (!response){
          throw new Error('Value expected!');
        }else{
          return response.result;
        }
      }),
      catchError((err)=>{
        throw new Error(err.message);
      })
    )
  }

  public getDataPage(endPoint:string, page:number): Observable<any>{
    return this.http.get(`${this.mySwapiUrl}/${endPoint}/page/${page}`).pipe(
      map((response:any)=>{
        if (!response){
          throw new Error('Value expected!');
        }else{
          return response.result;
        }
      }),
      catchError((err)=>{
        throw new Error(err.message);
      })
    )
  }

  public getDataId(endPoint:string,id:string): Observable<any>{
    return this.http.get(`${this.mySwapiUrl}/${endPoint}/${id}`).pipe(
      map((response:any)=>{
        if (!response){
          throw new Error('Value expected!');
        }else{
          return response;
        }
      }),
      catchError((err)=>{
        throw new Error(err.message);
      })
    )
  }

  public putDataId(endPoint:string,id:string,img:{}):Observable<any>{
    return this.http.put(`${this.mySwapiUrl}/${endPoint}/${id}`,img).pipe(
      map(response =>{
        if (!response){
          throw new Error('Value expected!');
        }else{
          return response;
        }
      }),catchError((err)=> {
        throw new Error(err.message);
      })
    )
  }
}
