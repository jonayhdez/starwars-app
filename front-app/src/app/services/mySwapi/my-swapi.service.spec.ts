import { TestBed } from '@angular/core/testing';

import { MySwapiService } from './my-swapi.service';

describe('MySwapiService', () => {
  let service: MySwapiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MySwapiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
