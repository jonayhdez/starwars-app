# Star Wars App 

Aplicación de información sobre el mundo Star Wars.
Consulta de datos de personajes, planetas,... e introducción de nueva información.

# Stack
MEAN

# API 
información principal obtenida de Swapi (API pública de Star Wars).
Creación de API propia en Nodejs para añadir nuevos campos de datos a la API publica y poder hacer CRUD.

# Arrancar proyecto

Instalar el back, ejecutar script de scrapeo de la API e iniciar el servidor.

cd back-API
npm i
node script-wrapper/scriptWrapper.js
npm run dev


Instalar e iniciar el front.

cd front-App
npm i   
ng serve
http://localhost:4200

# Back-API Enpoints
ip: http://localhost:3005

GET

/people "Todos los personajes"

/people/id "Personajes por id (por ejemplo: http://localhost:3005/people/1)"

/people/page/pageNumber "Personajes paginados de 10 en 10 (por ejemplo: http://localhost:3005/people/page/1)"

/planets "Todos los planetas"

/planets/id "Planetas por id (por ejemplo: http://localhost:3005/planets/1)"

/planets/page/pageNumber "Planetas paginados de 10 en 10 (por ejemplo: http://localhost:3005/planets/page/1)"



PUT

Personajes por id: people/id (body {_id + elementos a modificar})
